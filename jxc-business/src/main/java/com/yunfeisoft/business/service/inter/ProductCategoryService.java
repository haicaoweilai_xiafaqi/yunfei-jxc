package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.ProductCategory;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductCategoryService
 * Description: 商品类别service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface ProductCategoryService extends BaseService<ProductCategory, String> {

    public Page<ProductCategory> queryPage(Map<String, Object> params);

    public List<ProductCategory> queryList(Map<String, Object> params);

    public List<ProductCategory> queryByName(String orgId, String name);

    /**
     * 拖拽变换组织节点从属
     *
     * @param id       要变换的节点id
     * @param targetId 变换到目标节点id
     * @return
     */
    public int modifyWithDrag(String id, String targetId);
}