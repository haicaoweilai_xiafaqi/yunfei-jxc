package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.WarehouseUser;
import com.applet.utils.Page;

import java.util.Map;

/**
 * ClassName: WarehouseUserDao
 * Description: 仓库用户信息Dao
 * Author: Jackie liu
 * Date: 2020-08-04
 */
public interface WarehouseUserDao extends BaseDao<WarehouseUser, String> {

    public Page<WarehouseUser> queryPage(Map<String, Object> params);
}