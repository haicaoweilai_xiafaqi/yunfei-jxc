package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.Supplier;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: SupplierDao
 * Description: 供应商信息Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SupplierDao extends BaseDao<Supplier, String> {

    public Page<Supplier> queryPage(Map<String, Object> params);

    public List<Supplier> queryList(Map<String, Object> params);

    public List<Supplier> queryByName(String orgId, String name);

    public boolean isDupName(String orgId, String id, String name);
}