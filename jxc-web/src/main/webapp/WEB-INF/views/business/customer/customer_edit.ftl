<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑客户信息</title>
	<#include "/common/vue_resource.ftl">
	<script type="text/javascript" src="${params.contextPath}/js/Convert_Pinyin.js"></script>
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">名称<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="name" placeholder="请输入公司名称或者联系人姓名" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">联系人</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.linkman" placeholder="请输入联系人" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">类型</label>
				<div class="layui-input-block">
					<select v-model="record.type" class="layui-input">
						<option value="">请选择</option>
						<option value="1">零售客户</option>
						<option value="2">会员客户</option>
						<option value="3">批发客户</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">传真</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.fax" placeholder="请输入传真" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">会员卡号</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.memberNo" placeholder="请输入会员卡号" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">生日</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.birthDate" id="birthDate" placeholder="请输入生日" class="layui-input" readonly style="cursor:pointer"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">常用电话</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.phone" placeholder="请输入常用电话" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备用电话</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.standbyPhone" placeholder="请输入备用电话" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">地址</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.address" placeholder="请输入地址" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">QQ号</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.qq" placeholder="请输入QQ号" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">微信</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.wechat" placeholder="请输入微信" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">旺旺</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.wangwang" placeholder="请输入旺旺" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">Email</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.email" placeholder="请输入Email" class="layui-input"/>
				</div>
			</div>
			<#--<div class="layui-form-item">
				<label class="layui-form-label">预收余额</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.balance" placeholder="请输入预收余额" class="layui-input"/>
				</div>
			</div>-->
			<div class="layui-form-item">
				<label class="layui-form-label">拼音简码</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.pinyinCode" placeholder="请输入拼音简码" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea v-model="record.remark" placeholder="请输入备注" class="layui-textarea"></textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record : {
				id:'${params.id!}',
				code:'',
				name:'',
				linkman:'',
				type:'',
				fax:'',
				memberNo:'',
				birthDate:'',
				phone:'',
				standbyPhone:'',
				address:'',
				qq:'',
				wechat:'',
				wangwang:'',
				email:'',
				remark:'',
				//balance:'',
				pinyinCode:'',
			},
			name: ''
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		watch: {
			'name': function (newVal, oldVal) {
				this.record.name = newVal || "";
				if (!newVal) {
					this.record.pinyinCode = "";
					return false;
				}
				this.record.pinyinCode = pinyin.getCamelChars(newVal);
			}
		},
		methods: {
			init:function(){
				var that = this;
				laydate.render({elem: '#birthDate', type:'date', done:function (value) {
						that.record.birthDate = value;
					}});

			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/customer/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
					that.name = item.name || "";
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/customer/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>
