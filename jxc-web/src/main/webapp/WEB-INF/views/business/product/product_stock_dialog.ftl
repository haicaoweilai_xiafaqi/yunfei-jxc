<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>商品列表</title>
	<#include "/common/vue_resource.ftl">
    <style>
        body{background:#f2f2f2;}
        .app-container{margin:5px;}
        .app-header, .app-list{padding:5px 15px;}
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md3" style="visibility: hidden;">
                aa
            </div>
            <div class="layui-col-md9 text-right">
                <#if !(params.warehouseId??)>
                    <span style="margin-top:5px;display:inline-block;">仓库：</span>
                    <select v-model="params.warehouseId" class="layui-input" @change="loadData"/>
                        <option :value="item.id" v-for="(item, index) in warehouses" :key="item.id">{{item.name}}</option>
                    </select>
                </#if>
                <input type="text" v-model="params.namePinyin" placeholder="商品名称、简拼、条码" class="layui-input" @keyup.13="loadData"/>
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
                <button type="button" class="layui-btn layui-btn-sm search-button" @click="addProduct">添加商品</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">商品列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>商品条码</th>
                    <th>商品名称</th>
                    <th>商品规格</th>
                    <th>商品库存</th>
                    <th>所在仓库</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" @click="rowClick(index)" style="cursor:pointer;">
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.productCode}}</td>
                    <td>{{item.productName}}</td>
                    <td>{{item.productStandard}}</td>
                    <td>{{item.stock}}</td>
                    <td>{{item.warehouseName}}</td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="6" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                warehouseId:'${params.warehouseId!}',
                namePinyin:'',
                pinyinCode:'',
                page: 1,
            },
            warehouses:[],
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadWarehouses();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadWarehouses: function () {
                <#if !(params.warehouseId??)>
                    var that = this;
                    $.http.post("${params.contextPath}/web/warehouse/queryList.json", this.params).then(function (data) {
                        if (!data.success) {
                            $.message(data.message);
                            return;
                        }
                        that.warehouses = data.data;
                        that.params.warehouseId = that.warehouses[0].id;
                        that.loadData();
                    });
                <#else>
                    this.loadData();
                </#if>
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/warehouseProduct/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            rowClick: function (index) {
                var row = this.rows[index];
                <#if (params.multi)??>
                    parent.app.multiProductSelectCallBack(row);
                <#else>
                    parent.app.productSelectCallBack(row);
                </#if>
                $.message("该商品已选择");
            },
            addProduct:function () {
                this.showTypes = false;
                var url = "${params.contextPath!}/view/business/product/product_edit.htm";
                DialogManager.open({url:url, width:'80%', height:'100%', title:'添加商品'});
            },
        }
    });
</script>
</body>

</html>
