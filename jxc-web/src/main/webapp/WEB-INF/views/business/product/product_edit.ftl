<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑商品信息</title>
	<#include "/common/vue_resource.ftl">
	<script type="text/javascript" src="${params.contextPath}/js/Convert_Pinyin.js"></script>
	<style>
		.input-short{width:400px;display:inline-block;}
		.input-btn{height:36px;vertical-align:top;margin-left:10px;}
	</style>
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">条码</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.code" placeholder="没有条码可以不用填写，系统会自动生成条码，后期可修改" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">名称<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="name" placeholder="请输入名称" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">规格</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.standard" class="layui-input input-short"/>
					<button type="button" class="layui-btn input-btn layui-btn-normal" @click="showProductParamsDialog(1)">选择规格</button>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">单位</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.unit" class="layui-input input-short"/>
					<button type="button" class="layui-btn input-btn layui-btn-normal" @click="showProductParamsDialog(2)">选择单位</button>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">类别</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.categoryName" class="layui-input input-short" readonly/>
					<button type="button" class="layui-btn input-btn layui-btn-normal" @click="showProductCategoryDialog">选择类别</button>
					<button type="button" class="layui-btn input-btn layui-btn-primary" @click="clearProductCategory">清空</button>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">供应商</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.supplierName" class="layui-input input-short" readonly/>
					<button type="button" class="layui-btn input-btn layui-btn-normal" @click="showSupplierDialog">选择供应商</button>
					<button type="button" class="layui-btn input-btn layui-btn-primary" @click="clearSupplier">清空</button>
				</div>
			</div>
			<#--<div class="layui-form-item">
				<label class="layui-form-label">库存数量</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.stock" placeholder="请输入库存数量" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">缺货下限</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.shortageLimit" placeholder="请输入缺货下限" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">积压上限</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.backlogLimit" placeholder="请输入积压上限" class="layui-input"/>
				</div>
			</div>-->
			<div class="layui-form-item">
				<label class="layui-form-label">商品说明</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.intro" placeholder="请输入商品说明" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">进货价</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.buyPrice" placeholder="请输入进货价" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">零售价</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.salePrice" placeholder="请输入零售价" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">会员价</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.memberPrice" placeholder="请输入会员价" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">批发价</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.tradePrice" placeholder="请输入批发价" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">拼音简码</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.pinyinCode" placeholder="请输入拼音简码" class="layui-input"/>
				</div>
			</div>
			<#if !((params.id)??)>
				<div class="layui-form-item">
					<label class="layui-form-label">库存信息</label>
					<div class="layui-input-block">
						<table class="layui-table" lay-even lay-skin="nob" lay-size="sm">
							<thead>
							<tr>
								<th style="width:20px;">#</th>
								<th>仓库</th>
								<th>库存数量</th>
								<th>缺货下限</th>
								<th>积压上限</th>
							</tr>
							</thead>
							<tbody>
								<tr v-for="(item, index) in warehouseProducts">
									<td>{{1 + index}}</td>
									<td>{{item.warehouseName}}</td>
									<td><input type="text" v-model="item.stock" class="layui-input"/></td>
									<td><input type="text" v-model="item.shortageLimit" class="layui-input"/></td>
									<td><input type="text" v-model="item.backlogLimit" class="layui-input"/></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			<#else>
				<div class="layui-form-item">
					<label class="layui-form-label">库存信息</label>
					<div class="layui-input-block">
						<table class="layui-table" lay-even lay-skin="nob" lay-size="sm">
							<thead>
							<tr>
								<th style="width:20px;">#</th>
								<th>仓库</th>
								<th>初始库存数量</th>
								<th>缺货下限</th>
								<th>积压上限</th>
							</tr>
							</thead>
							<tbody>
							<tr v-for="(item, index) in warehouseProducts">
								<td>{{1 + index}}</td>
								<td>{{item.warehouseName}}</td>
								<td><input type="text" v-model="item.initStock" class="layui-input"/></td>
								<td><input type="text" v-model="item.shortageLimit" class="layui-input"/></td>
								<td><input type="text" v-model="item.backlogLimit" class="layui-input"/></td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</#if>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record: {
				id: '${params.id!}',
				code: '',
				name: '',
				standard: '',
				unit: '',
				categoryId: '',
				categoryName: '',
				supplierId: '',
				supplierName: '',
				//stock:'0',
				//shortageLimit:'0',
				//backlogLimit:'0',
				intro: '',
				buyPrice: '0',
				salePrice: '0',
				memberPrice: '0',
				tradePrice: '0',
				pinyinCode: '',
				warehouseProductsStr: '',
			},
			name: '',
			warehouseProducts:[]
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		watch: {
			'name': function (newVal, oldVal) {
				this.record.name = newVal || "";
				if (!newVal) {
					this.record.pinyinCode = "";
					return false;
				}
				this.record.pinyinCode = pinyin.getCamelChars(newVal);
			}
		},
		methods: {
			init:function(){
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/

			},
			loadData: function () {
				if (!'${params.id!}') {
					this.loadWarehouse();
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/product/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
					that.name = item.name || "";
					that.loadWarehouse();
				});
			},
			loadWarehouse: function () {
				var that = this;
				$.http.post('${params.contextPath}/web/warehouse/queryList.json').then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var list = data.data, results = [];
					for (var i = 0; i < list.length; i++) {
						var item = list[i];
						results.push({
							warehouseId: item.id,
							warehouseName: item.name,
							stock: item.stock || 0,
							initStock: item.initStock || 0,
							shortageLimit: 0,
							backlogLimit: 1
						});
					}
					that.warehouseProducts = results;
					that.loadWarehouseProduct()
				});
			},
			loadWarehouseProduct: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/warehouseProduct/queryList.json', {productId: "${params.id!}"}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var list = data.data, warehouseProducts = that.warehouseProducts;
					for (var i = 0; i < list.length; i++) {
						var item = list[i];
						for (var j = 0; j < warehouseProducts.length; j++) {
							var wp = warehouseProducts[j];
							if (item.warehouseId == wp.warehouseId) {
								wp.stock = item.stock;
								wp.initStock = item.initStock;
								wp.shortageLimit = item.shortageLimit;
								wp.backlogLimit = item.backlogLimit;
								break;
							}
						}
					}
				});
			},
			submitForm: function () {
				var warehouseProductsStr = this.warehouseProducts.length > 0 ? JSON.stringify(this.warehouseProducts) : "";
				this.record.warehouseProductsStr = warehouseProductsStr;
				$.http.post('${params.contextPath}/web/product/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
			showProductCategoryDialog: function () {
				var url = "${params.contextPath!}/view/business/productCategory/productCategory_dialog.htm";
				DialogManager.open({url: url, width: '90%', height: '100%', title: '选择类别'});
			},
			productCategorySelectCallBack: function (row) {
				this.record.categoryId = row.id;
				this.record.categoryName = row.name;
				DialogManager.closeAll();
			},
			clearProductCategory: function () {
				this.record.categoryId = "";
				this.record.categoryName = "";
			},
			showSupplierDialog: function () {
				var url = "${params.contextPath!}/view/business/supplier/supplier_dialog.htm";
				DialogManager.open({url: url, width: '90%', height: '100%', title: '选择供应商'});
			},
			supplierSelectCallBack: function (row) {
				this.record.supplierId = row.id;
				this.record.supplierName = row.name;
				DialogManager.closeAll();
			},
			clearSupplier: function () {
				this.record.supplierId = "";
				this.record.supplierName = "";
			},
			showProductParamsDialog: function (type) {
				var url = "${params.contextPath!}/view/business/productParams/productParams_dialog.htm?type=" + type;
				DialogManager.open({url: url, width: '90%', height: '90%', title: '选择商品参数'});
			},
			productParamsSelectCallBack: function (row) {
				if (row.type == 1) {
					this.record.standard = row.name;
				} else if (row.type == 2) {
					this.record.unit = row.name;
				}
				DialogManager.closeAll();
			},
		}
	});
</script>
</body>

</html>
