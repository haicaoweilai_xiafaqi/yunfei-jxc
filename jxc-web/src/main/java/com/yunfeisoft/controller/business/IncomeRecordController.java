package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.IncomeRecord;
import com.yunfeisoft.business.service.inter.IncomeRecordService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: IncomeRecordController
 * Description: 收款信息Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class IncomeRecordController extends BaseController {

    @Autowired
    private IncomeRecordService incomeRecordService;

    /**
     * 添加收款信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/incomeRecord/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(IncomeRecord record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "saleOrderId", "订单号为空");
        validator.number(request, "incomeAmount", "实收金额不合法");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        if (record.getIncomeAmount() == null || record.getIncomeAmount().compareTo(BigDecimal.ZERO) == 0) {
            return ResponseUtils.success("保存成功");
            //return ResponseUtils.warn("实收金额不能小于0");
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        incomeRecordService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 批量收款
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/incomeRecord/batchSave", method = RequestMethod.POST)
    @ResponseBody
    public Response batchSave(HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "saleOrderIds", "订单号为空");
        validator.required(request, "actualAmount", "实际收款为空");
        validator.number(request, "actualAmount", "实际收款格式非法");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        /*if (record.getIncomeAmount().compareTo(BigDecimal.ZERO) < 0) {
            return ResponseUtils.warn("实收金额不能小于0");
        }*/

        String actualAmount = ServletRequestUtils.getStringParameter(request, "actualAmount", null);
        String saleOrderIds = ServletRequestUtils.getStringParameter(request, "saleOrderIds", null);
        User user = ApiUtils.getLoginUser();

        String result = incomeRecordService.batchSave2(user.getOrgId(), saleOrderIds.split(","), new BigDecimal(actualAmount));
        if (StringUtils.isNotBlank(result)) {
            return ResponseUtils.warn(result);
        }
        return ResponseUtils.success("保存成功");
    }

    /**
     * 分页查询收款信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/incomeRecord/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String customerName = ServletRequestUtils.getStringParameter(request, "customerName", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("orgId", user.getOrgId());
        params.put("customerName", customerName);

        Page<IncomeRecord> page = incomeRecordService.queryPage(params);
        return ResponseUtils.success(page);
    }
}
